
The program uses the FLEX and BISON tools that are
implementations of the old LEX and YACC tools
which facilitate and assist the programmer in the translation process.

It is divided into three files that are compiled together,
using FLEX, BISON and GCC. to facilitate this process of
compilation, just run the "compila.sh" file:

$ ./compila.sh
And separately use the following commands:
$ lex lexico.l
$ yacc sintatico.y -d -y
$ gcc -c y.tab.c lex.yy.c
$ gcc y.tab.o lex.yy.o btoc.c -o btoc

for execution, just type:

$ ./btoc <entry.sh
$ ./btoc <entry_with_irrors.sh

Since the aim of the program is to translate bash script code to another equivalent code in C. It receives as input
a simple BASH script file, separates the tokens recognized by the
lexical analyzer, and then it generates a syntactic tree annotated by the
syntactic / semantic parser, and at the end, the equivalent C code is printed on screen
structured as a code in C.

![YaccLex](yacclex.png)